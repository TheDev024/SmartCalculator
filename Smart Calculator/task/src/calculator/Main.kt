package calculator

import java.math.BigInteger
import java.util.*

val scanner = Scanner(System.`in`)

val NUMBER_REGEX = "([-+]?\\d+)(\\.\\d+)?".toRegex()
val OPERATOR_REGEX = "[-+*/%^]".toRegex()
val VARIABLE_NAME_REGEX = "[a-zA-Z]+".toRegex()
val PARENTHESIS_REGEX = "[()]".toRegex()
val INVALID_OPERATORS = "[*/^]{2,}".toRegex()

val add = { a: BigInteger, b: BigInteger -> a + b }
val subtract = { a: BigInteger, b: BigInteger -> a - b }
val multiply = { a: BigInteger, b: BigInteger -> a * b }
val divide = { a: BigInteger, b: BigInteger -> a / b }
val power = { a: BigInteger, b: BigInteger -> a.toBigDecimal().pow(b.toInt()).toBigInteger() }

enum class Operator(val sign: String, val precedence: Int, val operation: (BigInteger, BigInteger) -> BigInteger) {
    ADDITION("+", 1, add), SUBTRACTION("-", 1, subtract), MULTIPLICATION("*", 2, multiply), DIVISION(
        "/", 2, divide
    ),
    POWER(
        "^", 3, power
    );
}

fun getOperator(sign: String): Operator? = Operator.values().find { it.sign == sign }

class SmartCalculator {

    private val helpMessage = """
        Commands
        /exit - exit the program
        /help - get help message
        Functions:
        Calculate expressions
        Assign variables
        Get variable names
        Notes on variables:
        1. Variable names must contain only Latin letters
        2. Variable name must contain more than one letter
        3. Variable names are case sensitive
        4. Variables can be reassigned
    """.trimIndent()
    private val variables: MutableMap<String, BigInteger> = mutableMapOf()

    fun start() {
        while (true) {
            var input = scanner.nextLine().trim() // remove redundant spaces from start and end
            input = input.replace("\\s+".toRegex(), " ") // remove redundant spaces in the string
            input = input.replace("(--)+-".toRegex(), "-") // remove redundant - signs
            // remove redundant + signs and convert BigDecimal - to +
            input = input.replace("(\\++|--)".toRegex(), "+")
            when {
                input.isEmpty() -> continue

                input[0] == '/' -> when (input.removePrefix("/")) {
                    "help" -> println(helpMessage)

                    "exit" -> break

                    else -> println("Unknown command")
                }

                input.isAssignment() -> if (input.isValidAssignment()) makeAssignment(input)

                else -> {
                    val result = input.isValidExpression()
                    if (result.first) println(calculate(result.second!!))
                }
            }
        }
        println("Bye!")
    }

    private fun makeAssignment(assignment: String) {
        val (variable, expression) = assignment.split("\\s*=\\s*".toRegex())
        val result = expression.isValidExpression() // check for validity of expression
        if (result.first) variables[variable] = calculate(result.second!!)
    }

    private fun calculate(postfixNotation: Stack<Any>): BigInteger {
        val calculationStack = Stack<Any>()
        while (postfixNotation.isNotEmpty()) calculationStack.push(postfixNotation.pop())
        val resultStack = Stack<BigInteger>()
        while (calculationStack.isNotEmpty()) {
            val element = calculationStack.pop()
            when {
                element is String && element.isOperand() -> resultStack.push(
                    if (element.isNumber()) element.toBigInteger()
                    else variables[element]
                )

                else -> {
                    val operator = (element as Operator).operation
                    val b = resultStack.pop()
                    val a = resultStack.pop()
                    val result = operator(a, b)
                    resultStack.push(result)
                }
            }
        }
        return resultStack.pop()
    }

    private fun String.isValidExpression(): Pair<Boolean, Stack<Any>?> {
        if (this.contains(INVALID_OPERATORS)) {
            println("Invalid expression")
            return Pair(false, null)
        }
        val expressionVariables = this.getVariables()
        val infixNotation = this.toInfixNotation()
        val postfixNotation = infixToPostfixNotation(infixNotation)
        if (expressionVariables.any { !it.isValidVariable() }) {
            println("Invalid identifier")
            return Pair(false, null)
        }
        if (expressionVariables.any { !variables.containsKey(it) }) {
            println("Unknown variable")
            return Pair(false, null)
        }
        return Pair(postfixNotation != null, postfixNotation)
    }

    private fun infixToPostfixNotation(infixNotation: MutableList<String>): Stack<Any>? {
        val processStack = Stack<Any>()
        val resultStack = Stack<Any>()
        for (element in infixNotation) when {
            element.isOperand() -> resultStack.push(element)

            element.isOperator() -> if (processStack.empty() || processStack.peek() == "(") processStack.push(
                getOperator(element)!!
            ) else {
                val operator = getOperator(element)!!
                var topOperator = processStack.peek() as Operator
                if (operator.precedence <= topOperator.precedence) while (processStack.isNotEmpty() && processStack.peek() != "(") {
                    topOperator = processStack.peek() as Operator
                    if (topOperator.precedence < operator.precedence) break
                    resultStack.push(processStack.pop())
                }
                processStack.push(operator)
            }

            else -> if (element == "(") processStack.push(element) else {
                while (processStack.peek() != "(") {
                    resultStack.push(processStack.pop())
                    if (processStack.empty()) {
                        println("Invalid expression")
                        return null
                    }
                }
                processStack.pop()
            }
        }
        while (processStack.isNotEmpty()) {
            if (processStack.peek() == "(") {
                println("Invalid expression")
                return null
            }
            resultStack.push(processStack.pop())
        }
        return resultStack
    }

    private fun String.isOperand(): Boolean = this.isValidVariable() || this.isNumber()

    private fun String.toInfixNotation(): MutableList<String> {
        val infixNotation: MutableList<String> = mutableListOf()
        var expression = this
        expression = if (expression[0].toString().isValidVariable()) {
            infixNotation.add(VARIABLE_NAME_REGEX.find(expression)!!.value)
            expression.replaceFirst(VARIABLE_NAME_REGEX.find(expression)!!.value, "")
        } else {
            infixNotation.add(NUMBER_REGEX.find(expression)!!.value)
            expression.replaceFirst(NUMBER_REGEX.find(expression)!!.value, "")
        }
        while (expression.isNotEmpty()) {
            val stringStart = expression[0].toString()
            expression = when {
                stringStart.isParenthesis() || stringStart.isOperator() -> {
                    infixNotation.add(stringStart)
                    if (stringStart == "(") {
                        expression = expression.replaceFirst(stringStart, "").trim()
                        if (expression[0].toString().isValidVariable()) {
                            infixNotation.add(VARIABLE_NAME_REGEX.find(expression)!!.value)
                            expression.replaceFirst(VARIABLE_NAME_REGEX.find(expression)!!.value, "")
                        } else {
                            infixNotation.add(NUMBER_REGEX.find(expression)!!.value)
                            expression.replaceFirst(NUMBER_REGEX.find(expression)!!.value, "")
                        }
                    } else expression.replaceFirst(stringStart, "")
                }

                stringStart.isValidVariable() -> {
                    val variable = VARIABLE_NAME_REGEX.find(expression)!!.value
                    infixNotation.add(variable)
                    expression.replaceFirst(variable, "")
                }

                else -> {
                    val number = NUMBER_REGEX.find(expression)!!.value
                    infixNotation.add(number)
                    expression.replaceFirst(number, "")
                }
            }
            expression = expression.trim()
        }
        return infixNotation
    }

    private fun String.isOperator(): Boolean = OPERATOR_REGEX.matches(this)

    private fun String.isParenthesis(): Boolean = PARENTHESIS_REGEX.matches(this)

    private fun String.isValidAssignment(): Boolean {
        // separate variable name and expression
        val (variable, expression) = this.split("\\s*=\\s*".toRegex(), 2)
        // get variables from expression
        val expressionVariables = expression.getVariables()
        return if (expression.contains("=") || expressionVariables.any { !it.isValidVariable() }) { // check for validity of assignment and expression variables
            println("Invalid assignment")
            false
        } else if (!variable.isValidVariable()) { // check for validity of variable name
            println("Invalid identifier")
            false
        } else true
    }

    private fun String.isValidVariable(): Boolean = VARIABLE_NAME_REGEX.matches(this)

    private fun String.getVariables(): List<String> =
        this.replace(PARENTHESIS_REGEX, " ").replace(OPERATOR_REGEX, " ").trim().split("\\s+".toRegex())
            .filter { !it.isNumber() }

    private fun String.isNumber(): Boolean = NUMBER_REGEX.matches(this)

    private fun String.isAssignment(): Boolean = ".+=.+".toRegex().matches(this)
}

fun main() {
    val calculator = SmartCalculator()
    calculator.start()
}
